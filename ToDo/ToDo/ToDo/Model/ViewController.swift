//
//  ViewController.swift
//  ToDo
//
//  Created by Christian on 8/5/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var itemManager = ItemManager()

    override func viewDidLoad() {
        
        


    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            if section == 0 {
                return itemManager.toDoItems.count
            }
        
        return itemManager.doneItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! itemTableViewCell
        
        if indexPath.section == 0{
            cell.titleLabel.text = itemManager.toDoItems[indexPath.row].title
            cell.locationLabel.text = itemManager.toDoItems[indexPath.row].title
        } else {
            
            cell.titleLabel.text = itemManager.doneItems[indexPath.row].title
            cell.locationLabel.text = itemManager.doneItems[indexPath.row].title
        }
        
        
        return cell
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            performSegue(withIdentifier: "toInfoViewSegue", sender: self)}
        
    }

    
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "To Do: " : "Done: "
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddItemSegue"{
        let destination = segue.destination as! AddItemViewController
        destination.itemManager = itemManager
        }
        if segue.identifier == "toInfoViewSegue"{
            let destination = segue.destination as! ItemInfoViewController
            let selectedRow = itemsTableView.indexPathsForSelectedRows![0]
            destination.itemInfo = (itemManager, selectedRow.row)
        }
        
    }
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return indexPath.section == 0 ? "Check" : "UnCheck"
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //return indexPath.section == 0
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //itemManager.checkItem(index: indexPath.row)
        
        if indexPath.section==0{
            itemManager.checkItem(index: indexPath.row)
        } else {
            itemManager.unCheckItem(index: indexPath.row)
        
        }
        itemManager.updateArrays()
       itemsTableView.reloadData()
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        itemManager.updateArrays()
        itemsTableView.reloadData()
    }

    @IBOutlet weak var itemsTableView: UITableView!
    

}

