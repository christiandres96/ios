//
//  AddItemViewController.swift
//  ToDo
//
//  Created by Christian on 9/5/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {
    
    
    
    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var locationTextField: UITextField!
    
    @IBOutlet weak var descriptionTextFiel: UITextField!
    
    
    var itemManager:ItemManager!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let itemTitle = titleTextField.text ?? ""
        let itemLocation = locationTextField.text ?? ""
        let itemDescription = descriptionTextFiel.text ?? ""
        
        if (titleTextField.text == "" || descriptionTextFiel.text == " "){
            //print("Empty fields, insert data !!  ")
            showAlert(title: "ERROR", message: "Title and Description is required")
            navigationController?.popViewController(animated: true)
        }else{
            
            //print("Data saved")
            //showAlert(title: "Succed", message: "Data Saved")
            
            /*let item = Item(
                id: UUID(),
                title: itemTitle,
                location: itemLocation,
                description: itemDescription
            )*/
            
            
            
            //itemManager.toDoItems += [item]
            itemManager?.addItem(
                title: itemTitle,
                location: itemLocation,
                itemDescription: itemDescription)
            
            navigationController?.popViewController(animated: true)
            
        }
        
        
        
    }
    
    
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title:title,message:message,preferredStyle: .alert)
        
        let  okAction = UIAlertAction(title:"OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated:true,completion: nil)
        
    }
    
    
}


/*

let alert = UIAlertController(title: "My Alert", message: "El campo locacion esta vacio", preferredStyle: .alert)
alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
    NSLog("The \"OK\" alert occured.")
}))
self.present(alert, animated: true, completion: nil)*/
