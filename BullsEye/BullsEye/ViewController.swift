//
//  ViewController.swift
//  BullsEye
//
//  Created by Christian on 24/4/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var targetLabel: UILabel!
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    
    @IBOutlet weak var roundLabel: UILabel!
    
    @IBOutlet weak var gameSlider: UISlider!
    
    /*
    var target = 0
    var score = 0
    var roundGame = 0*/
    let gameModel = Game()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //restarGame()
        gameModel.restarGame()
        
    }
    
    
    @IBAction func playButtonPressed(_ sender: Any) {
        
        let sliderValue = Int(gameSlider.value)
        gameModel.play(sliderValue: sliderValue)
        
       /* switch sliderValue {
        case target:
            score += 100
        case (target - 2)...(target + 2):
            score += 50
        case (target - 5)...(target + 5):
            score += 10
        default:
            break
        }
        
        roundGame += 1
        target = Int(arc4random_uniform(100))*/
        
        /*scoreLabel.text = "\(score)"
        targetLabel.text =  "\(target)"
        roundLabel.text =  "\(roundGame)"*/
    }
    
    @IBAction func restarButtonPressed(_ sender: Any) {
        gameModel.restarGame()
        setValues()
    }
    
    
    func restarGame(){
        /*target = Int(arc4random_uniform(100))
        score=0
        scoreLabel.text = "\(score)"
        targetLabel.text = "\(target)"
        roundLabel.text = "1"
        */
        
        
    }
 
    @IBAction func infoButtonPressed(_ sender: Any) {
    }
    
    
    @IBAction func WinnerButtonPressed(_ sender: Any) {
        /*if score > 100 {
            performSegue(withIdentifier: "toWinnerSegue", sender: self)*/
            if gameModel.score > 100 {
                performSegue(withIdentifier: "toWinnerSegue", sender: self)
        }
        
    }
    func setValues(){
        targetLabel.text = "\(gameModel.target)"
        scoreLabel.text = "\(gameModel.score)"
        roundLabel.text = "\(gameModel.roundGame)"
    }
    
    
}

